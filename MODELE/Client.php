<?php

    class Client {

        // Attributs
        private $nomClient;
        private $prenomClient;
        private $telephoneClient;
        private $emailClient;
        private $departement;
        private $categorie;

        // Constructeur
        public function __construct($_nomClient, $_prenomClient, $_telephoneClient, $_emailClient, $_departement, $_categorie) {
            $this->nomClient = $_nomClient;
            $this->prenomClient = $_prenomClient;
            $this->telephoneClient = $_telephoneClient;
            $this->emailClient = $_emailClient;
            $this->departement = $_departement;
            $this->categorie = $_categorie;
        }

        // Methode
        public function getNomClient() {
            return $nomClient;
        }

        public function getPrenomClient() {
            return $prenomClient;
        }

        public function getTelephoneClient() {
            return $telephoneClient;
        }

        public function getEmailClient() {
            return $emailClient;
        }

        public function decrire() {
            echo ( 'Le client '.$this->nomClient . ' ' . $this->prenomClient . ' tel : ' . $this->telephoneClient . ' email : ' . $this->emailClient . ' cate : ' . $this->categorie->getNomCategorie()) . ' dep : ' . $this->departement->getNomDepartement() . ' ' . $this->departement->getNumeroDepartement();
        }
    }

?>