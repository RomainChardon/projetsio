<?php

    class Departement {

        // Attributs
        private $nomDepartement;
        private $numeroDepartement;

        // Constructeur
        public function __construct($_nomDepartement, $_numeroDepartement) {
            $this->nomDepartement = $_nomDepartement;
            $this->numeroDepartement = $_numeroDepartement;
        }

        // Methode
        public function getNomDepartement() {
            return $this->nomDepartement;
        }

        public function getNumeroDepartement() {
            return $this->numeroDepartement;
        }
    }

?>