<?php

    class Categorie {

        // Attributs
        private $nomCategorie;

        // Constructeur
        public function __construct($_nomCategorie) {
            $this->nomCategorie = $_nomCategorie;
        }

        // Methode
        public function getNomCategorie() {
            return $this->nomCategorie;
        }
    }

?>