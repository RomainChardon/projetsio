<?php

require('../CONTROLEUR/connexionBDD.php');

?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../VUE/body.css">
    <title>Immobien</title>
</head>
<body>
    <header><h1>IMMOBIEN</h1></header>
    <form action="" method="POST">
        <h3>Recherche de client :</h3>
        <div class=" conteneur_nom conteneur">
        <label for="nom">Nom du client :</label>
        <input type="text" name="nom" id="nom">
        <input type="submit" value="Rechercher" name="n">
        </div>
          <!-- box categorie -->
          <div class="conteneur_categorie conteneur">
        <label for="categorie">Categorie de client :</label>
        <select name="categorie" id="categorie">
        <option value=""></option>
            <?php
                $compteur = 0;
                $envoye = $bdd->prepare('SELECT nomCategorie FROM categorie');
                $envoye->execute();
                $compteur = $envoye->columnCount();

                while($data = $envoye->fetch()) {
                    for ($i=0; $i < $compteur; $i++) {
                        echo ('<option value="'.$data[$i].'">' . $data[$i] . '</option>');
                    }
                } 
            ?>
        </select>
        <input type="submit" value="Rechercher" name="cat">
        </div>
        <!-- box departement -->
        <div class="conteneur_dep conteneur">    
        <label for="departement">Département du client :</label>
        <select name="departement" id="departement">
            <option value=""></option>
            <?php
                $compteur = 0;
                $envoye = $bdd->prepare('SELECT nomDepartement FROM departement');
                $envoye->execute();
                $compteur = $envoye->columnCount();

                while($data = $envoye->fetch()) {
                    for ($i=0; $i < $compteur; $i++) {
                        echo ('<option value="'.$data[$i].'">' . $data[$i] . '</option>');
                    }
                } 
            ?>
        </select>
        <input type="submit" value="Rechercher" name="dep">
        </div>
        <!-- box mail -->
        <div class="conteneur_mail conteneur">
        <label for="mail">L'adresse Email :</label>
        <input type="text" name="email" id="mail">
        <input type="submit" value="Rechercher" name="mail">
        </div>
    </form>

    <div class="tableau">
        <?php
                
            if (isset($_POST['n'])){
                $req_nom = $_POST['nom'];
                $requete = "SELECT * FROM client WHERE nomClient = '$req_nom'";
                $test = $bdd->prepare($requete);
                $test->execute();
                $envoye = $bdd->query($requete);
                $compteur = $test->columnCount();
                ?>
                <table>
                <?php
                    for($i=0;$i<$compteur;$i++){
                        echo"<td>".$envoye->getColumnMeta($i)["name"]."</td>";
                    }
                    echo "</tr>";
                    while ($row = $envoye->fetch()){
                        echo "<tr>";
                    for($i=0;$i<$compteur;$i++){
                        echo "<td>".$row[$i]."</td>";
                    }
                    echo "</tr>";
                    }
                
                    ?>
                    </table> <?php }
            

            if (isset($_POST['cat'])){

                $compteur = 0;
                $req_nom = $_POST['categorie'];
                $requete = "SELECT * from client INNER JOIN categorie ON categorie.idCategorie = client.refCategorie WHERE nomCategorie = '$req_nom'";
                $test = $bdd->prepare($requete);
                $test->execute();
                $envoye = $bdd->query($requete);
                $compteur = $test->columnCount();
                ?>
                <table>
                <?php
                    for($i=0;$i<$compteur;$i++){
                        echo"<td>".$envoye->getColumnMeta($i)["name"]."</td>";
                    }
                    echo "</tr>";
                    while ($row = $envoye->fetch()){
                        echo "<tr>";
                    for($i=0;$i<$compteur;$i++){
                        echo "<td>".$row[$i]."</td>";
                    }
                    echo "</tr>";
                    }
                
                    ?>
                    </table> <?php }
                    
            
            

            if (isset($_POST['dep'])) {
                $compteur = 0;
                $req_nom = $_POST['departement'];
                $requete = "SELECT * from client INNER JOIN departement ON departement.idDepartement = client.refDepartement WHERE nomDepartement = '$req_nom'";
                $test = $bdd->prepare($requete);
                $test->execute();
                $envoye = $bdd->query($requete);
                $compteur = $test->columnCount();
                ?>
                <table>
                <?php
                    for($i=0;$i<$compteur;$i++){
                        echo"<td>".$envoye->getColumnMeta($i)["name"]."</td>";
                    }
                    echo "</tr>";
                    while ($row = $envoye->fetch()){
                        echo "<tr>";
                    for($i=0;$i<$compteur;$i++){
                        echo "<td>".$row[$i]."</td>";
                    }
                    echo "</tr>";
                    }
                
                    ?>
                    </table> <?php }
            

            if (isset($_POST['mail'])) {
                $req_nom = $_POST['email'];
                $requete = "SELECT * FROM client WHERE emailClient = '$req_nom'";
                $test = $bdd->prepare($requete);
                $test->execute();
                $envoye = $bdd->query($requete);
                $compteur = $test->columnCount();
                ?>
                <table>
                <?php
                    for($i=0;$i<$compteur;$i++){
                        echo"<td>".$envoye->getColumnMeta($i)["name"]."</td>";
                    }
                    echo "</tr>";
                    while ($row = $envoye->fetch()){
                        echo "<tr>";
                    for($i=0;$i<$compteur;$i++){
                        echo "<td>".$row[$i]."</td>";
                    }
                    echo "</tr>";
                    }
                
                    ?>
                    </table> <?php }
        ?>
    </div>    
</body>
</html>